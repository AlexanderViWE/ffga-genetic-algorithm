import numpy
from unittest import TestCase
from ga.pareto import pareto_ranks


class ParetoRanksTest(TestCase):
    def test_pareto_ranks(self):
        fitness = [[0, 0], [1, 0], [0, 1], [2, 1], [1, 2]]
        ranks = pareto_ranks(numpy.array(fitness))
        self.assertEqual(list(ranks), [3, 2, 2, 1, 1])

        fitness.append([10, 10])
        ranks = pareto_ranks(numpy.array(fitness))
        self.assertSequenceEqual(list(ranks), [4, 3, 3, 2, 2, 1])

        fitness = [[0, 0], [0, 0]]
        ranks = pareto_ranks(numpy.array(fitness))
        self.assertEqual(list(ranks), [1, 1])

        fitness = [[0, 0], [1, 1]]
        ranks = pareto_ranks(numpy.array(fitness))
        self.assertEqual(list(ranks), [2, 1])
