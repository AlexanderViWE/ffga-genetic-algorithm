# from os.path import join, dirname

from setuptools import setup, find_packages

import ga

if __name__ == '__main__':
    setup(
        name='ga',
        version=ga.__version__,
        description='Implementation of a genetic algorithm with support for '
                    'multi-criteria optimization based on the Pareto set.',
        # long_description=open(join(dirname(__file__), 'README.md')).read(),
        license='MIT License',
        url='https://gitlab.com/AlexanderViWE/ffga-genetic-algorithm',
        package_dir={'': 'src'},
        packages=find_packages('src', include=[
            'ga*',
        ]),
        data_files=[('', ['LICENSE'])],
        install_requires=[
            "alive-progress",
            "numpy",
        ],
    )
