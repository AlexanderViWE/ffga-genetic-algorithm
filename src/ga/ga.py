"""
Реализация генетического алгоритма.
"""

from typing import Callable, Any, List, Union, Iterable

from ga.callback import *
from ga.individual import Individual, Population, GeneticDecoder, AsIsGeneticDecoder
from ga.operators import ParentSelector, CrossingGenes, GeneMutator
from ga.pareto import pareto_ranks


class FitnessEstimator:
    """
    Алгоритм вычисления функции пригодности для особей из популяции.
    """

    def __init__(self):
        self.coder = None

    def set_gene_coder(self, coder: GeneticDecoder):
        self.coder = coder

    def fitness(self, representation: Any) -> numpy.ndarray:
        """
        Вычисление функции пригодности для особи.
        """
        raise NotImplementedError()

    def fitness_all(self, population: Population):
        """
        Вычисление функции пригодности для каждой особи.
        """
        raise NotImplementedError()

    def metrics_names(self) -> List[str]:
        """
        Получить список имен функций пригодности.
        """
        raise NotImplementedError()


class SimpleFitnessEstimator(FitnessEstimator):
    def __init__(self, fitness_func: Callable):
        super().__init__()
        self._fitness_func = fitness_func

    def fitness(self, representation: Any) -> numpy.ndarray:
        fitness = self._fitness_func(representation)
        fitness = numpy.reshape(fitness, (1,))
        assert fitness >= 0, f"Fitness '{fitness}' should not be negative"
        return fitness

    def fitness_all(self, population: Population):
        for individual in population:
            if individual.fitness is None:
                representation = self.coder.from_genes(individual.genes)
                fitness = self.fitness(representation)
                individual.fitness = fitness
                individual.criteria = fitness

    def metrics_names(self) -> list:
        return [self._fitness_func.__name__, ]


class ParetoFitnessEstimator(FitnessEstimator):
    """
    Алгоритм вычисления функции пригодности для особей из популяции.
    """

    def __init__(self, funcs: list[Callable]):
        super().__init__()
        self.__fitness_funcs = funcs

    def fitness(self, representation: Any) -> numpy.ndarray:
        result = []
        for func in self.__fitness_funcs:
            fitness = func(representation)
            assert fitness >= 0, f"Fitness '{fitness}' should not be negative"
            result.append(fitness)
        return numpy.array(result)

    def fitness_all(self, population: List[Individual]):
        fitness = []
        # Вычисляем значений функций пригодности особей.
        for individual in population:
            if individual.fitness is None:
                representation = self.coder.from_genes(individual.genes)
                individual.fitness = self.fitness(representation)
            fitness.append(individual.fitness)
        fitness = numpy.array(fitness)

        # Вычисляем критерия для отбора родителей.
        criteria = pareto_ranks(fitness, False).astype(int)
        for i in range(len(population)):
            individual = population[i]
            if individual.criteria is None:
                individual.criteria = criteria[i]

    def metrics_names(self) -> list:
        return [func.__name__ for func in self.__fitness_funcs]


# TODO: Добавить метрики.
class GeneticOptimizer:
    """
    Генетический алгоритм.
    """

    def __init__(self,
                 estimator: Union[Callable, List[Callable], FitnessEstimator],
                 parent_selector: ParentSelector,
                 crossing: CrossingGenes,
                 mutator: GeneMutator = None,
                 *,
                 # metrics=None,
                 genes_coder: GeneticDecoder = None,
                 population_size: int = 20,
                 save_k_parent: int = 0):
        """
        :param genes_coder: Кодировщик генов.
        :param estimator: Оператор оценки приспособленности особи.
        :param parent_selector: Оператор выбора родителей для скрещивания.
        :param crossing: Оператор скрещивания генов особи.
        :param mutator: Оператор мутации генов особи.
        :param population_size: Размер популяции.
        """
        # Параметры популяции
        self.population_size = population_size
        self.save_k_parent = save_k_parent

        # Генетические операторы
        self._estimator = self._process_estimator(estimator)
        self._parent_selector = parent_selector
        self._crossing = crossing
        self._mutator = mutator

        if genes_coder is None:
            genes_coder = AsIsGeneticDecoder()
        self.genes_coder = genes_coder
        self._estimator.set_gene_coder(self.genes_coder)
        # self.metrics = metrics or []

        # Переменные для управления алгоритмом
        self.history = None
        self.stop_train = False
        self.population = Population()

    @staticmethod
    def _process_estimator(estimator_arg) -> FitnessEstimator:
        if isinstance(estimator_arg, FitnessEstimator):
            estimator = estimator_arg
        elif callable(estimator_arg):
            estimator = SimpleFitnessEstimator(estimator_arg)
        else:
            estimator = ParetoFitnessEstimator(estimator_arg)
        return estimator

    def load_population_genes(self, genes: Iterable[numpy.ndarray]):
        self.population.clear()
        population = []
        for gene in genes:
            population.append(Individual(gene))
        self.population.extend(population)

    def load_population(self, population: Iterable[Any]) -> None:
        """
        Загрузка существующих решений.
        """
        genes = map(self.genes_coder.to_genes, population)
        self.load_population_genes(genes)

    def generate_initial_population(self, generator: Callable[[], Any]) -> None:
        """
        :param generator: Функция создания нового индивида.
        :return: None
        """
        population = [generator() for _ in range(self.population_size)]
        self.load_population(population)

    def optimize(self,
                 iterations: int = 1000,
                 verbose: bool = True,
                 callbacks: list = None) -> StatisticHistory:
        """
        Оптимизация функций. Запуск ГА.

        :param iterations: Количество эпох.
        :param verbose:
        :param callbacks:
        :return: Количество прошедших эпох.
        """

        if len(self.population) == 0:
            raise ValueError("Initial population not created.")
        else:
            self.population = self._evaluate_population(self.population)

        if not isinstance(callbacks, CallbackList):
            callbacks = [] if callbacks is None else callbacks
            callbacks = CallbackList(callbacks,
                                     ga_model=self,
                                     population=self.population,
                                     add_progress_bar=verbose,
                                     iterations=iterations)

        callbacks.on_train_begin()

        for _ in range(iterations):
            self.step()

            metrics = dict(zip(self._estimator.metrics_names(),
                               self.population.get_fitness().T))
            callbacks.set_population(self.population)
            callbacks.on_evaluated(metrics)

            if self.stop_train:
                break

        callbacks.on_train_end()

        return self.history

    def step(self):
        """
        Эпоха. Шаг эволюции.
        """

        # Получение потомков для текущего поколения.
        children = self._breeding(self.population)
        children = list(map(Individual, children))
        next_population = Population(children)
        if self.save_k_parent > 0:
            next_population += self.population[-self.save_k_parent:]
        self.population = next_population

        # Оценка нового поколения
        self.population = self._evaluate_population(self.population)

        assert len(self.population) == self.population_size, f"{len(self.population)}"
        assert all([i.criteria is not None for i in self.population])

    def _breeding(self, population):
        """
        Выведение потомков.
        """

        # Выбор родителей и скрещивание.
        parents = self._parent_selector.get_parents(population)
        genes_list = self._crossing.cross_all([i.get_genes() for i in parents])
        # Пытаемся подвергнуть мутации гены потомков.
        if self._mutator is not None:
            genes_list = self._mutator.mutate_all(genes_list)
        return genes_list

    def _evaluate_population(self, population) -> Population:
        """
        Оценка приспособленности индивидов.
        """

        self._estimator.fitness_all(population)
        # Сортируем популяцию так, чтобы первыми были худшие особи,
        # а последними были лучшие особи.
        population.sort(key=lambda x: x.criteria)
        population = population[-self.population_size:]
        return population
