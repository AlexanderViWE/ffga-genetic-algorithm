import copy
import pickle
from typing import Optional

import numpy
from alive_progress import alive_bar


class CallbackList:

    def __init__(self, callbacks=None, ga_model=None, population=None, add_progress_bar=False, iterations=None):
        self.ga_model = ga_model
        self.population = population

        # ???
        self.iterations = iterations

        if callbacks is None:
            callbacks = []
        self.callbacks = callbacks
        self._add_default_callbacks(add_progress_bar)

        if ga_model:
            self.set_ga_model(ga_model)
        if population:
            self.set_population(population)

    def set_ga_model(self, ga_model) -> None:
        self.ga_model = ga_model
        for callback in self.callbacks:
            callback.set_ga_model(ga_model)

    def set_population(self, population) -> None:
        self.population = population
        for callback in self.callbacks:
            callback.set_population(population)

    def on_train_begin(self):
        """"""
        for callback in self.callbacks:
            callback.on_train_begin()

    def on_train_end(self):
        """"""
        for callback in self.callbacks:
            callback.on_train_end()

    # def on_parents_selected(self, selected_parents):
    #     """"""
    #     for callback in self.callbacks:
    #         callback.on_parents_selected(selected_parents)
    #
    # def on_crossover(self, offspring):
    #     """"""
    #     for callback in self.callbacks:
    #         callback.on_crossover(offspring)
    #
    # def on_mutated(self, offspring):
    #     """"""
    #     for callback in self.callbacks:
    #         callback.on_mutated(offspring)

    def on_evaluated(self, fitness=None):
        """"""
        for callback in self.callbacks:
            callback.on_evaluated(fitness)

    def _add_default_callbacks(self, add_progress_bar: bool):
        progbar = None
        history = None
        for callback in self.callbacks:
            if isinstance(callback, ProgresBar):
                progbar = callback
            elif isinstance(callback, StatisticHistory):
                history = callback

        if progbar is None and add_progress_bar:
            self.callbacks.append(ProgresBar(self.iterations))
        if history is None and add_progress_bar:
            self.callbacks.append(StatisticHistory())


class Callback:

    def __init__(self):
        self.population = None
        self.ga_model = None

    def set_population(self, population):
        self.population = population

    def set_ga_model(self, ga_model):
        self.ga_model = ga_model

    def on_train_begin(self):
        """"""

    def on_train_end(self):
        """"""

    # def on_parents_selected(self, selected_parents=None):
    #     """"""
    #
    # def on_crossover(self, offspring=None):
    #     """"""
    #
    # def on_mutated(self, offspring=None):
    #     """"""

    def on_evaluated(self, fitness=None):
        """"""


class ProgresBar(Callback):

    def __init__(self, iterations: int):
        super().__init__()
        self.iterations = iterations
        self.bar = self._create_progres_bar()

    def on_evaluated(self, fitness=None):
        next(self.bar)

    def on_train_end(self):
        self.bar = None

    def _create_progres_bar(self):
        with alive_bar(self.iterations) as bar:
            while bar.current != self.iterations:
                yield bar()


class StatisticHistory(Callback):
    """
    Функции сбора статистики во время работы генетического алгоритма.
    """

    def __init__(self):
        super().__init__()

        self.statistic_funcs = {
            "mean": lambda x: numpy.mean(x, axis=0),
            "std": lambda x: numpy.std(x, axis=0),
            "min": lambda x: numpy.min(x, axis=0),
            "max": lambda x: numpy.max(x, axis=0),
            "median": lambda x: numpy.median(x, axis=0),
        }
        self.statistic = {}

    def set_ga_model(self, ga_model):
        super().set_ga_model(ga_model)
        ga_model.history = self

    def on_train_begin(self):
        self.statistic = {}

    def on_evaluated(self, metrics=None):
        metrics = metrics or {}
        for metric_name in metrics.keys():
            metric_history = self.statistic.get(metric_name, {})

            for func_name, func in self.statistic_funcs.items():
                history = metric_history.get(func_name, [])
                history.append(func(metrics[metric_name]))
                metric_history[func_name] = history

            self.statistic[metric_name] = metric_history

    def on_train_end(self):
        self.ga_model.history = self


# class BestFitnessTracking:
#
#     def __init__(self):
#         self.best_fitness: Optional[numpy.ndarray] = None
#
#     def check_best(self, population: List[Individual]) -> Tuple[bool, Individual]:
#         # Ожидается, что индивиды отсортированы по пригодности
#         individual = population[-1]
#
#         if self.best_fitness is None or all(individual.fitness >= self.best_fitness):
#             self.best_fitness = copy.deepcopy(individual.fitness)
#             return True, individual
#         return False, individual


class SaveBestGenes(Callback):

    def __init__(self, filename):
        super().__init__()
        self.best_fitness: Optional[numpy.ndarray] = None
        self.best_genes = None

        self.filename = filename

    def on_evaluated(self, fitness=None):
        individual = self.population[-1]
        if self.best_fitness is None or all(individual.fitness >= self.best_fitness):
            self.best_fitness = copy.deepcopy(individual.fitness)
            self.best_genes = copy.deepcopy(individual.genes)
            self._save_solution()

        # # Получаем лучших особей в эпохе
        # best_group = []
        # for i in self.population:
        #     if i.criteria == self.population[0].criteria:
        #         best_group.append(i)
        #
        # best_in_population = []
        # for i in best_group:
        #     best_in_population.append(i.fitness)
        #
        # self._iter_bests.extend(best_in_population)

    def _save_solution(self):
        with open(self.filename, "wb") as file:
            pickle.dump(self.best_genes, file)


class EarlyStop(Callback):

    def __init__(self,
                 max_without_improvement: int = None,
                 target_fitness: numpy.ndarray = None):
        """
        :param max_without_improvement: Максимальное количество эпох
                                        без улучшения приспособленности.
        :param target_fitness: Значение приспособленность, при которой
                               алгоритм завершается.
        """
        super().__init__()
        self.max_without_improvement = max_without_improvement
        self.target_fitness = target_fitness

        self.without_improvement = 0
        self.best_fitness = None

    def on_evaluated(self, fitness=None):
        fitness = numpy.array(self.population[-1].fitness)
        if self.best_fitness is None or all(fitness > self.best_fitness):
            self.best_fitness = copy.deepcopy(fitness)
            self.without_improvement = 0
        else:
            self.without_improvement += 1

        if self._check_to_stop():
            self.ga_model.stop_train = True

    def _check_to_stop(self):
        if self.without_improvement >= self.max_without_improvement:
            return True
        if (self.target_fitness is not None
                and self.best_fitness is not None
                and all(self.best_fitness >= self.target_fitness)):
            return True
        return False
