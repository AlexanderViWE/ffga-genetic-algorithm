__version__ = '0.1'

from .ga import SimpleFitnessEstimator, ParetoFitnessEstimator, GeneticOptimizer
