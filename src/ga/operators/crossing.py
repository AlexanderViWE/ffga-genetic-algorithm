"""
Операторы скрещивания.
"""

from typing import List

import numpy


class CrossingGenes:
    """
    Оператор скрещивания генов индивидов.
    """

    def cross(self, parents_genes: list) -> list:
        """
        Алгоритм скрещивания генов.

        :param parents_genes: Набор родителей.
        :return: Набор потомков.
        """
        raise NotImplementedError()

    def cross_all(self, parents_list: List[List[numpy.ndarray]]) -> list:
        """
        Применить оператор скрещивание для претендентов.

        :param parents_list:
        :return:
        """
        children = []
        for parents in parents_list:
            children.extend(self.cross(parents))
        return children


class GeneMeanCrossing(CrossingGenes):

    def cross(self, parents_genes: list) -> list:
        return [sum(parents_genes) / len(parents_genes), ]


class TwistingCrossing(CrossingGenes):
    """
    Перекручивает гены родителей.
    """

    def __init__(self, both=False):
        self.both = both

    def cross(self, parents_genes: list) -> list:
        first, second = parents_genes

        # Выбираем точку перекручивания генов
        cross_position = numpy.random.randint(1, len(first) - 2)

        children = [self._cross(first, second, cross_position), ]
        if self.both:
            children.append(self._cross(second, first, cross_position))
        return children

    @staticmethod
    def _cross(parent_1, parent_2, cross_pos) -> numpy.ndarray:
        # Копируем гены первого родителя
        new_genes = numpy.copy(parent_1)
        # Копируем гены второго родителя
        new_genes[cross_pos:] = parent_2[cross_pos:]
        return new_genes


class DoubleTwistingCrossing(CrossingGenes):
    """"""

    def __init__(self, both=False):
        self.both = both

    def cross(self, parents_genes: list) -> list:
        first, second = parents_genes

        # Выбираем точки перекручивания генов
        pos_1, pos_2 = numpy.random.randint(0, len(first) - 1, size=2)
        if pos_1 > pos_2:
            pos_1, pos_2 = pos_2, pos_1

        children = [self._cross(first, second, pos_1, pos_2), ]
        if self.both:
            children.append(self._cross(second, first, pos_1, pos_2))
        return children

    @staticmethod
    def _cross(parent_1, parent_2, pos_1, pos_2) -> numpy.ndarray:
        # Копируем гены первого родителя
        new_genes = numpy.copy(parent_1)
        # Копируем гены второго родителя
        new_genes[pos_1:pos_2] = parent_2[pos_1:pos_2]
        return new_genes


class TspTwistingCrossing(CrossingGenes):
    """
    Для задачи коммивояжера. Перекручивает гены родителей.
    """

    def cross(self, parents_genes: list) -> list:
        first, second = parents_genes

        # Создаем новые гены
        new_genes = numpy.full(len(first), numpy.nan)

        # Выбираем точку перекручивания генов
        cross_pos = numpy.random.randint(1, len(first) - 2)

        # Копируем гены первого родителя
        new_genes[:cross_pos] = first[:cross_pos]

        # Копируем гены второго родителя, которых еще нет
        index = cross_pos
        for gene in second[cross_pos:]:
            if gene not in new_genes:
                new_genes[index] = gene
                index += 1

        # Копируем гены первого родителя, которых еще нет
        for gene in first[cross_pos:]:
            if gene not in new_genes:
                new_genes[index] = gene
                index += 1

        assert index == len(first)
        assert len(set(new_genes)) == len(new_genes)

        # TODO: Создавать 2 потомка
        return [new_genes.astype(int), ]
