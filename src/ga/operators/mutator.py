"""
Операторы мутации генов.
"""

import random

import numpy


class GeneMutator:
    """
    Оператор мутации генов индивида.

    Рекомендуется, что каждый ген имеет малый шанс мутации:
    chance = 0.05 / len(genes)
    """

    def mutate(self, genes: numpy.ndarray) -> numpy.ndarray:
        """
        Попытаться мутировать.
        """
        raise NotImplementedError()

    def get_chance(self) -> float:
        """
        Шанс мутации одного гена.
        """
        raise NotImplementedError()

    def set_chance(self, value: float) -> None:
        """
        Установить шанс мутации одного гена.
        """
        raise NotImplementedError()

    def mutate_all(self, genes_list: list) -> list:
        return [self.mutate(i) for i in genes_list]


class AddingNoiseGeneMutator(GeneMutator):

    def __init__(self, chance: float, sigma: float = 1):
        """
        :param chance: 0.001 <= x <= 0.01
        :param sigma: Среднеквадратическое отклонение для функции
                      случайных чисел.
        """
        assert 0 <= chance <= 1
        assert sigma >= 0
        self._chance = chance
        self.sigma = sigma

    def get_chance(self) -> float:
        return self._chance

    def set_chance(self, value: float) -> None:
        self._chance = value

    def mutate(self, genes: numpy.ndarray) -> numpy.ndarray:
        for i in range(len(genes)):
            if random.random() < self.get_chance():
                genes[i] += random.gauss(0, self.sigma)
        return genes


class SwapGenesMutator(GeneMutator):
    """
    Каждый ген может поменяться местами с другим.
    """

    def __init__(self, chance=0.05):
        assert 0 <= chance <= 1
        self._chance = chance

    def get_chance(self) -> float:
        return self._chance

    def set_chance(self, value: float) -> None:
        self._chance = value

    def mutate(self, genes: numpy.ndarray) -> numpy.ndarray:
        mutates_count = len(genes)
        chance = self._chance / mutates_count

        for _ in range(mutates_count):
            if numpy.random.random() < chance:
                first_index = numpy.random.randint(0, len(genes) - 1)
                second_index = numpy.random.randint(0, len(genes) - 1)
                genes[first_index], genes[second_index] = genes[second_index], genes[first_index]
        return genes
