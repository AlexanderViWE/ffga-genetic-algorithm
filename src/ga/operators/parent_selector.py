"""
Операторы выбора родителей для скрещивания.
"""

import math
import random
from typing import List

import numpy

from ga.individual import Population


class ParentSelector:
    """Оператор выбора успешных особей для скрещивания.

    Вероятность выбора особи пропорциональна его приспособленности.
    """

    def get_parents(self, population: Population) -> Population:
        """Получение списка особей для скрещивания.

        :param population: Список особей.
        :return: Список особей для скрещивания.
        """
        raise NotImplementedError()


class NParentProportionalSelector(ParentSelector):
    def __init__(self, parents: int, n: int = 2):
        """
        :param parents: Число наборов родителей.
        :param n: Число отбираемых особей.
        """
        self._parents = parents
        self._n = n

    def get_parents(self, population: Population) -> List[Population]:
        # Шансы выбора каждой особи
        chances = self._calc_chances(population)

        # Сумма вероятностей должна быть ~= 1.0
        assert math.isclose(sum(chances), 1.), f"sum={sum(chances)}"

        parents = []
        for _ in range(self._parents):
            sample = Population()
            for _ in range(self._n):
                parent = random.choices(population, chances)[0]
                sample.append(parent)
            parents.append(sample)

        return parents

    @staticmethod
    def _calc_chances(population) -> numpy.ndarray:
        """
        Вычисление шанса быть выбранным для каждой особи по приспособленности.
        """
        chances = numpy.array([i.criteria for i in population], dtype=float)
        # chances = chances - min(chances) + 1
        return chances / sum(chances)


class TournamentSelector(ParentSelector):
    """
    Турнирный выбор родителей.
    """

    def __init__(self, parents: int, n: int = 2, group_size: int = 3, replace=False):
        """
        :param parents: Число наборов родителей.
        :param n: Число отбираемых особей.
        :param group_size: Размер группы, внутри которой будет выбрана лучшая особь.
        :param replace: Группа может содержать одинаковых индивидов.
        """
        self._parents = parents
        self._n = n
        self.group_size = group_size
        self.replace = replace

    def get_parents(self, population: Population) -> List[Population]:
        parents = []
        for _ in range(self._parents):
            sample = Population()
            for _ in range(self._n):
                sample.append(self._round(population))
            parents.append(sample)

        return parents

    def _round(self, candidates):
        """
        Алгоритм выбора 1 родителя:
        1) Выбрать 'group_size' особей.
        2) Среди этих особей выбрать лучшую.
        """
        group = numpy.random.choice(candidates, self.group_size, self.replace)
        best = None
        best_criteria = float("-inf")
        for i in group:
            if i.criteria > best_criteria:
                best_criteria = i.criteria
                best = i
        return best


# class NParentRankSelector:
#     def __init__(self, n):
#         self.__n = n
#         self._current_population = None
#         self._chances = None
#
#     @staticmethod
#     def calc_chances(population) -> numpy.ndarray:
#         size = len(population)
#         chances = numpy.arange(size) + 1
#         chances = 2 * chances / (size * (size + 1))
#         return chances
#
#     def get_parents(self, population: Population) -> Population:
#         # size = len(population)
#         # self._chances = numpy.arange(size) + 1
#         # self._chances = 2 * self._chances / (size * (size + 1))
#         self._current_population = population
#         self._chances = self.calc_chances(population)
#
#         res = Population([])
#         for _ in range(self.__n):
#             res.append(self.calc_parent())
#         return res
#
#     def calc_parent(self) -> Individual:
#         indexes = numpy.argsort(self._current_population.get_fitness())
#         # indexes = numpy.arange(len(self._current_population))
#         first = random.choices(indexes, self._chances)[0]
#         return self._current_population[first]

def softmax(vector: numpy.ndarray) -> numpy.ndarray:
    e_shifted = numpy.exp(vector - numpy.max(vector))
    return e_shifted / e_shifted.sum()
