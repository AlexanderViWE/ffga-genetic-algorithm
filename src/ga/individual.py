from typing import Any

import numpy


class GeneticDecoder:
    """Кодировщик генов.

    Кодирует/декодирует представление объекта предметной области в гены.
    """

    def from_genes(self, genes: numpy.ndarray) -> Any:
        """Гены -> Представление объекта предметной области"""

    def to_genes(self, representation: Any) -> numpy.ndarray:
        """Представление объекта предметной области -> Гены"""


class AsIsGeneticDecoder(GeneticDecoder):
    def from_genes(self, genes: numpy.ndarray) -> Any:
        return genes

    def to_genes(self, representation: Any) -> numpy.ndarray:
        return representation


class Individual:
    """Индивид / Особь"""

    def __init__(self, genes: numpy.ndarray):
        """
        :param genes: Представление генов.
        """
        self.genes = genes

        # Значения функций пригодности индивида.
        # Чем больше, тем лучше.
        self.fitness: numpy.ndarray = None

        # Оценка индивида для ранжирования при выборе родителей.
        # Чем больше, тем выше шанс быть выбранной для скрещивания.
        self.criteria: float = None

    def reset_score(self) -> None:
        """
        Сброс оценки приспособленности.
        """
        self.fitness = None
        self.criteria = None


class Population(list):
    def __init__(self, population=(), decoder: GeneticDecoder = None):
        super(Population, self).__init__(population)
        self.decoder = decoder or AsIsGeneticDecoder()

    def get_fitness(self):
        return numpy.array([i.fitness for i in self], dtype=float)

    def get_criteria(self):
        return numpy.array([i.criteria for i in self], dtype=float)

    def get_genes(self):
        return [i.genes for i in self]

    def __getitem__(self, item):
        result = super().__getitem__(item)
        if isinstance(item, slice):
            return Population(result)
        return result
