import numpy


def pareto_ranks(scores: numpy.ndarray, dominant: bool = True):
    """Ранжирования индивидов,
    где ранг каждого индивида определяется числом доминирующих его индивидов.
    """

    ranks = numpy.zeros(len(scores), dtype=float)

    # Определяем ранг каждого индивида как число доминирующих его индивидов.
    for i in range(len(scores)):
        tmp = scores > scores[i]
        ranks[i] = tmp.all(axis=1).sum() + 1

    if not dominant:
        # Определяем ранг каждого индивида как количество индивидов имеющих
        # лучшее или равное ему значения приспособленности.
        ranks = len(ranks) - ranks + 1

    return ranks
