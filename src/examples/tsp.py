import matplotlib
import numpy

from draw import plot_fitness
from ga.callback import EarlyStop
from ga.ga import GeneticOptimizer, SimpleFitnessEstimator
from ga.operators import TournamentSelector, SwapGenesMutator, TspTwistingCrossing
from utils.traveling_salesman import plot_tsp, generate_tsp


def create_generator(size: int):
    def generate():
        array = numpy.arange(size)
        numpy.random.shuffle(array)
        return array

    return generate


def create_fit_func(data):
    def distance_between(first: numpy.ndarray, second: numpy.ndarray):
        return numpy.linalg.norm(first - second)

    def fitness_func(solution):
        assert len(solution) == len(data)

        distance = 0

        for index in range(1, len(solution)):
            first_index = solution[index - 1]
            second_index = solution[index]
            distance += distance_between(data[first_index], data[second_index])

        assert distance < 1e10
        return 1e10 - distance

    return fitness_func


def main():
    matplotlib.use('TkAgg')

    data = generate_tsp(15, scale=1000)
    generator = create_generator(len(data))

    estimator = SimpleFitnessEstimator(create_fit_func(data))
    population_size = 50
    max_iter = 1000

    opt = GeneticOptimizer(
        estimator,
        TournamentSelector(population_size, group_size=4),
        TspTwistingCrossing(),
        mutator=SwapGenesMutator(0.07),
        population_size=population_size,
        save_k_parent=0,
    )
    opt.generate_initial_population(generator)

    history = opt.optimize(
        max_iter,
        callbacks=[
            EarlyStop(40),
        ]
    )

    plot_tsp(data, solution=opt.genes_coder.from_genes(opt.population[-1].genes))

    # Графики приспособленности во время эволюции
    plot_fitness(history.statistic["fitness_func"])


if __name__ == '__main__':
    main()
