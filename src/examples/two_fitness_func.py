import matplotlib
import matplotlib.pyplot as plt
import numpy

from draw import plot_func, plot_fitness
from ga.ga import GeneticOptimizer, ParetoFitnessEstimator
from ga.operators import AddingNoiseGeneMutator, NParentProportionalSelector, GeneMeanCrossing


def fit_func1(vec):
    """
    Оптимизируемая функция 1.

    Абсолютный минимум: 0, 0
    """
    x, y = vec
    # tmp = x ** 2 + y ** 2 - 2 * numpy.cos(x ** 2 + y ** 2)
    tmp = 0.1 * x ** 2 + 0.1 * y ** 2 - 4 * numpy.cos(0.8 * x) - 4 * numpy.cos(0.8 * y) + 8
    return 1e4 - tmp


def fit_func2(vec):
    """
    Оптимизируемая функция 2.

    Абсолютный минимум: 0, 0
    """
    x, y = vec
    tmp = 10 / (0.005 * (x ** 2 + y ** 2)
                - numpy.cos(x) * numpy.cos(y / numpy.sqrt(2))
                + 2)
    tmp = - tmp + 10
    return 20 - tmp


def generator():
    """
    Сгенерировать случайное решение для задачи оптимизации.
    """
    solution = numpy.random.random(2) - 0.5
    solution = solution * 100 + numpy.array([100, 100], dtype=float)
    return solution


def main():
    matplotlib.use('TkAgg')

    funcs = [fit_func1, fit_func2]

    # Отрисовка графиков функций
    zone = (-10, 10)
    for f in funcs:
        plot_func(f, zone, zone)

    # Настройка алгоритма оптимизации для тестового запуска
    estimator = ParetoFitnessEstimator(funcs)
    population_size = 50
    max_iter = 200

    opt = GeneticOptimizer(
        estimator,
        NParentProportionalSelector(population_size),
        GeneMeanCrossing(),
        mutator=AddingNoiseGeneMutator(0.1, 5),
        population_size=population_size,
    )
    opt.generate_initial_population(generator)
    history = opt.optimize(max_iter)

    # Одна из лучших особей
    print(f"One of the best solutions: {opt.population[-1]}")

    # Графики приспособленности во время эволюции
    fig = plt.figure()
    row = 1
    for func_name, statistics in history.statistic.items():
        ax = fig.add_subplot(len(history.statistic), 1, row)
        plot_fitness(statistics, ax)
        ax.set_title(f'{func_name} {row}')
        row += 1
    plt.show()

    # TODO: Реализовать возможность отрисовать Парето фронт.
    # iterations = numpy.array(iter_bests[::10])
    # x = iterations[:, 0]
    # y = iterations[:, 1]
    # plt.scatter(x, y, color='k', marker='x', s=14)
    # plt.show()


if __name__ == '__main__':
    main()
