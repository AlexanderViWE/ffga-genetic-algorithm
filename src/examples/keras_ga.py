import keras
import matplotlib
import numpy

from draw import plot_fitness
from ga.ga import GeneticDecoder, GeneticOptimizer, EarlyStop
from ga.operators.crossing import TwistingCrossing
from ga.operators.mutator import AddingNoiseGeneMutator
from ga.operators.parent_selector import NParentProportionalSelector


class KerasDecoder(GeneticDecoder):

    def __init__(self, model):
        self.model = model

    def from_genes(self, genes: numpy.ndarray) -> object:
        solution_weights = self.model_weights_as_matrix(model=self.model, weights_vector=genes)
        _model = keras.models.clone_model(self.model)
        _model.set_weights(solution_weights)
        return _model

    def to_genes(self, representation: keras.models.Model) -> numpy.ndarray:
        weights_vector = []
        for layer in representation.layers:
            if layer.trainable:
                for l_weights in layer.get_weights():
                    weights = numpy.reshape(l_weights, newshape=(l_weights.size,))
                    weights_vector.extend(weights)
        return numpy.array(weights_vector)

    @staticmethod
    def model_weights_as_matrix(model, weights_vector):
        weights_matrix = []

        start = 0
        for layer_idx, layer in enumerate(model.layers):
            layer_weights = layer.get_weights()
            if layer.trainable:
                for l_weights in layer_weights:
                    layer_weights_shape = l_weights.shape
                    layer_weights_size = l_weights.size

                    layer_weights_vector = weights_vector[start:start + layer_weights_size]
                    layer_weights_matrix = numpy.reshape(layer_weights_vector, newshape=layer_weights_shape)
                    weights_matrix.append(layer_weights_matrix)

                    start = start + layer_weights_size
            else:
                for l_weights in layer_weights:
                    weights_matrix.append(l_weights)

        return weights_matrix


loss = keras.losses.MeanSquaredError()


def fitness_func(model: keras.Model):
    global data_inputs, data_outputs

    predictions = model.predict(data_inputs, verbose=0)

    error = loss(data_outputs, predictions).numpy() + 1e-6
    solution_fitness = 1. / error

    return solution_fitness


def math_func(vec):
    x, y, z = vec
    return 1. + (x ** 2 / 5 + y * 3 - z * 5) / 300.


# Data inputs
data_inputs = numpy.random.random(163 * 3) * 10
data_inputs = numpy.reshape(data_inputs, (163, 3))
print(data_inputs)

# Data outputs
data_outputs = numpy.array(list(map(math_func, data_inputs)))
print(data_outputs)


def main():
    matplotlib.use('TkAgg')

    # Create the Keras model.
    x = input_layer = keras.layers.Input(3)
    x = keras.layers.Dense(3, activation="relu")(x)
    x = keras.layers.Dense(3, activation="relu")(x)
    output_layer = keras.layers.Dense(1, activation="linear")(x)
    model = keras.Model(inputs=input_layer, outputs=output_layer)

    def generator():
        model_ = keras.models.clone_model(model)
        model_.build(3)
        return model_

    num_generations = 100
    population_size = 32
    save_k_parent = 0

    opt = GeneticOptimizer(
        fitness_func,
        parent_selector=NParentProportionalSelector(population_size - save_k_parent),
        crossing=TwistingCrossing(),
        mutator=AddingNoiseGeneMutator(0.05, 1),
        population_size=population_size,
        genes_coder=KerasDecoder(model),
        save_k_parent=save_k_parent
    )
    opt.generate_initial_population(generator)
    history = opt.optimize(
        num_generations,
        callbacks=[EarlyStop(20)],
    )

    # Графики приспособленности во время эволюции
    plot_fitness(history.statistic['fitness_func'])

    predictions = opt.genes_coder.from_genes(opt.population[-1].genes).predict(data_inputs)
    abs_error = loss(data_outputs, predictions).numpy()
    print(f"Loss: {abs_error}")


if __name__ == '__main__':
    main()
