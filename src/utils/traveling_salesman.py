"""
Задача коммивояжера / Travelling salesman problem (TSP).
"""

from typing import Iterable

import matplotlib
import matplotlib.pyplot as plt
import numpy


def generate_tsp(n_cities: int = 40, cord_dim: int = 2, scale: int = 100) -> numpy.ndarray:
    """
    Генерация набора городов для решения задачи коммивояжера.

    :param n_cities: Число городов.
    :param cord_dim: Размерность координат города.
    :param scale: Масштаб карты.
    :return: Массив точек размера (n_cities, cord_dim).
    """
    points = numpy.random.random(n_cities * cord_dim) * scale
    points = numpy.reshape(points, (n_cities, cord_dim))
    return points


def plot_tsp(cites: numpy.ndarray, solution: Iterable[int] = None) -> None:
    """
    Нарисовать график городов.
    """
    fig = plt.figure()
    ax = fig.add_subplot()
    ax.scatter(cites[:, 0], cites[:, 1])

    if solution is not None:
        path = numpy.take(cites, solution, axis=0)
        ax.plot(path[:, 0], path[:, 1], color="g", label=f"Solution", linestyle='dashed', linewidth=1)
        ax.legend()
    plt.show()


def _main():
    matplotlib.use('TkAgg')

    a = generate_tsp(5)
    plot_tsp(a)

    solution = list(range(10))
    plot_tsp(a, solution)


if __name__ == '__main__':
    _main()
