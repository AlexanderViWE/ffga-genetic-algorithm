import random
from typing import Dict

import matplotlib.pyplot as plt
import numpy


def plot_func(func, xinterval=(-1, 1), yinterval=(-1, 1), n: int = 20) -> None:
    """
    Отрисовка графика функции.
    """

    fig = plt.figure()
    ax = fig.add_subplot(projection='3d')

    xspace = numpy.linspace(xinterval[0], xinterval[1], n)
    yspace = numpy.linspace(yinterval[0], yinterval[1], n)
    xgrid, ygrid = numpy.meshgrid(xspace, yspace)
    z = []
    for x in xspace:
        for y in yspace:
            z.append(func((x, y)))
    zgrid = numpy.array(z).reshape(xgrid.shape)

    ax.plot_surface(xgrid, ygrid, zgrid)
    plt.show()


def plot_func_heightmap(func, xinterval=(-1, 1), yinterval=(-1, 1), n: int = 20) -> None:
    """
    Отрисовка карты высот.
    """

    xspace = numpy.linspace(xinterval[0], xinterval[1], n)
    yspace = numpy.linspace(yinterval[0], yinterval[1], n)
    xgrid, ygrid = numpy.meshgrid(xspace, yspace)

    zgrid = []
    for x in xspace:
        for y in yspace:
            zgrid.append(func((x, y)))
    zgrid = numpy.array(zgrid).reshape((n, n)).T

    plt.pcolor(xspace, yspace, zgrid)
    plt.colorbar()
    plt.show()


def chances_test_polt(chances=None, size=10, ntest=100_000):
    """
    Гистограмма распределения
    """

    if chances is None:
        assert size > 0
        chances = numpy.arange(size) + 1
        chances = 2 * chances / (size * (size + 1))
    else:
        size = len(chances)
    assert abs(sum(chances) - 1) <= 0.000001, abs(sum(chances) - 1)

    items = [i for i in range(size)]

    x = []
    for _ in range(ntest):
        x.append(random.choices(items, chances)[0])

    plt.hist(x)
    plt.show()


def plot_fitness(statistic: Dict[str, list], ax=None):
    """
    Графики приспособленности во время эволюции
    """

    mean_line = numpy.array(statistic['mean'])
    max_line = numpy.array(statistic['max'])
    min_line = numpy.array(statistic['min'])

    boarder = min(mean_line)
    min_line[min_line < boarder] = boarder

    def _draw(ax):
        x = numpy.arange(len(statistic['mean']))
        ax.plot(x, max_line, color="g", label=f"max fitness", linestyle='dashed', linewidth=1)
        ax.plot(x, min_line, color="r", label=f"min fitness", linestyle='dashed', linewidth=1)
        ax.fill_between(x, min_line, max_line, alpha=0.3)
        ax.plot(x, mean_line, color="k", label=f"mean fitness")
        ax.legend()

    if ax is None:
        fig = plt.figure()
        ax = fig.add_subplot()
        _draw(ax)
        plt.show()
    else:
        _draw(ax)
